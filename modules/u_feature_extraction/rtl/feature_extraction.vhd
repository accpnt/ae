library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity feature_extraction is
    generic (
        G_DATA_BITS     : natural := 18;
        G_THRESHOLD_MAX : natural := 5;
        G_THRESHOLD_MIN : natural := 2;
        G_IDLE_TIMEOUT  : natural := 5
    ); 
    port (
        clk  : in  std_logic;
        rst  : in  std_logic;
        din  : in  std_logic_vector(G_DATA_BITS - 1 downto 0);
        dout : out std_logic_vector(G_DATA_BITS - 1 downto 0)
    );
end entity feature_extraction;

architecture feature_extraction_arch of feature_extraction is

-- signals 
signal IT : std_logic := '0';
signal threshold_crossed : std_logic := '0';
signal peak_amplitude : std_logic_vector(G_DATA_BITS - 1 downto 0) := (others => '0');
signal idle_count : natural := 0;
signal crossings_count : natural := 0;

begin 
    threshold_crossing_detect_process : process (clk, rst)
    begin 
        if rst = '0' then
            dout <= (others => '0');
        elsif rising_edge(clk) then  -- rising clock edge
            IT <= '0';  -- we want IT to be a pulse
            if to_integer(unsigned(din)) < G_THRESHOLD_MIN or G_THRESHOLD_MAX < to_integer(unsigned(din)) then
                IT <= '1';
                threshold_crossed <= '1';
                idle_count <= 0;

                -- din is greater than the stored max amplitude
                if peak_amplitude <= din then
                    peak_amplitude <= din;
                end if;
            else
                idle_count <= idle_count + 1;

                if G_IDLE_TIMEOUT < idle_count then 
                    -- reset all signals
                    peak_amplitude <= (others => '0');
                    threshold_crossed <= '0';
                end if;

            end if;
        end if;
        dout <= peak_amplitude;
    end process;

    threshold_crossing_count_process : process (clk, rst)
    begin 
        if rst = '0' then
            crossings_count <= 0;
        elsif rising_edge(clk) then  -- rising clock edge
            if threshold_crossed = '1' then
                crossings_count <= crossings_count + 1;
            else
                crossings_count <= 0;
            end if;
        end if;
    end process;
end architecture feature_extraction_arch;