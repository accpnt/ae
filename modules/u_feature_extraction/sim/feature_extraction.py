import numpy
import matplotlib.pyplot as plt

f   = 5
w   = 2 * numpy.pi * f 
phi = 3
t = numpy.linspace(-15, 15, 100) # 100 linearly spaced numbers
y = numpy.sin(w * t - phi)/(w * t - phi)  # computing the values of sin(x)/x

y_u16 = y.astype(uint16)

# TODO find a way to have the results converted to 
# a range of values from 14/16 bits 0x0000 to 0xFFFF 
# where 0x0000 would be -3V and 0xFFFF would be 10V

# TODO display figure if only there's a switch
# use getops to take switch arguments

threshold_min = -0.02
threshold_max = 0.02
idle_timeout  = 10

def feature_extraction(din):
	it = 0
	idle_count = 0
	risetime = 0
	peak_amplitude = 0
	duration = 0
	for din_current in din: 
		if din_current < threshold_min or threshold_max < din_current : 
			it = 1
			idle_count = 0

			if abs(peak_amplitude) <= abs(din_current): 
				peak_amplitude = din_current
				print("peak_amplitute :" , peak_amplitude)

		else:
			idle_count = idle_count + 1
			if idle_timeout < idle_count: 
				print("IDLE")
				# reset all signals
			else:
				risetime = risetime + 1

if __name__ == '__main__':
	feature_extraction(y)

	# compose plot
	plt.title('Digital filter frequency response')
	plt.plot(t, y, 'ro')
	plt.plot(t, y, 'b')
	plt.ylabel('Amplitude Response (dB)')
	plt.xlabel('Frequency (rad/sample)')
	plt.grid()
	plt.show()
