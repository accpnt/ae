package fir_pkg is
	-- VHDL-2008 users will have to comment this line out as it's
	-- a default part of that standard
	type integer_vector is array(natural range <>) of integer;
end package fir_pkg;