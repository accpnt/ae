library ieee;
use ieee.std_logic_1164.all;
use work.fir_pkg.all;

entity fir is
    generic (
        G_TAPS : integer_vector
    ); 
    port (
        clk      : in  std_logic;
        rst      : in  std_logic;
        sample   : in  integer;
        filtered : out integer := 0
    );
end entity fir;

architecture fir_arch of fir is
begin 
    filtering : process (clk, rst)
        variable delay_line : integer_vector(0 to G_TAPS'length - 1) := (others => 0);
        variable sum : integer;
    begin 
        if rst = '0' then
            delay_line := (others => 0);
            sum        := 0;
            filtered   <= 0;
        elsif rising_edge(clk) then  -- rising clock edge
            delay_line := sample & delay_line(0 to G_TAPS'length - 2);
            sum := 0;
            for i in 0 to G_TAPS'length - 1 loop
                -- y[n] = h[n] * x[n - k]
                sum := sum + delay_line(i) * G_TAPS(G_TAPS'high - i);
            end loop;
            filtered <= sum;
        end if;
    end process;
end architecture fir_arch;