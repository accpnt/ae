library ieee;
use ieee.std_logic_1164.all;
use work.fir_pkg.all;

entity fir_test is
end entity fir_test;

architecture fir_test_arch of fir_test is
    -- constants
    constant C_LOW_PASS_TAPS  : integer_vector := ( 1, 1, 1, 1, 1);
    constant C_HIGH_PASS_TAPS : integer_vector := (-1, 0, 1);
    constant C_SAMPLES        : integer_vector := (0,0,0,0,1,1,1,1,1);

    -- components
    component fir
        generic (
            G_TAPS : integer_vector
        ); 
        port (
            clk      : in  std_logic;
            rst      : in  std_logic;
            sample   : in  integer;
            filtered : out integer := 0
        );
    end component fir;
    
    -- signals 
    signal s_sample   : integer;
    signal s_filtered : integer;
    signal s_clk      : std_logic := '1';
    signal s_rst      : std_logic := '1';
    signal s_finished : std_logic;
    
    for fir_object : fir use entity work.fir(fir_arch);
begin  
    fir_object : fir
        generic map (
            G_TAPS => C_LOW_PASS_TAPS  -- try other taps in here
        )
        port map (
            clk      => s_clk,
            rst      => s_rst,
            sample   => s_sample,
            filtered => s_filtered
        );

    -- clock generation
	clk_process : process
	begin
		s_clk <= '0'; 
		wait for 50 ns;
		s_clk <= '1';
		wait for 50 ns;
	end process clk_process;
        
    -- waveform generation
    wavegen_process : process
    begin
        s_finished <= '0';
        for i in C_SAMPLES'range loop
            s_sample <= C_SAMPLES(i);
            wait until rising_edge(s_clk);
        end loop;
        -- allow pipeline to empty - input will stay constant
        for i in 0 to 5 loop
            wait until rising_edge(s_clk);
        end loop;
        s_finished <= '1';
        report (time'image(now) & " Finished");
        wait;
    end process wavegen_process;
    
end architecture fir_test_arch;