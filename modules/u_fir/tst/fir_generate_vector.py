import numpy
import matplotlib.pyplot as plt

f   = 5
w   = 2 * numpy.pi * f 
phi = 3
t = numpy.linspace(0, 15, 100) # 100 linearly spaced numbers
y = numpy.sin(w * t - phi)/(w * t - phi)  # computing the values of sin(x)/x

# TODO find a way to have the results converted to 
# a range of values from 14/16 bits 0x0000 to 0xFFFF 
# where 0x0000 would be -3V and 0xFFFF would be 10V

# TODO display figure if only there's a switch
# use getops to take switch arguments

# compose plot
plt.title('Digital filter frequency response')
plt.plot(t, y, 'ro')
plt.plot(t, y, 'b')
plt.ylabel('Amplitude Response (dB)')
plt.xlabel('Frequency (rad/sample)')
plt.grid()
plt.show()
